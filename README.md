# Welcome

> Documentation is the set of all **documents**, which are **all sources** containing information to help make decisions, communicate decisions taken and **record matters of interest to the organization**.

# Information

> We are a flexible, free and easy to use container-based cloud platform, we offer developers a simple and quick way to get their Bots Discord online 24/7.

We rely on donor collaboration to stay on the air and improve the quality of our services. We have no relationship with Discord Inc.



`Tech Support:` support@discloudbot.com
•

`Trust & Safety:` abuse@discloudbot.com
•

`Twitter:` <https://twitter.com/discloudbot>
•

`YouTube:` <https://youtube.com/c/pedroricardor>
•

`Discord:` <https://discord.gg/CvxevT5>

---